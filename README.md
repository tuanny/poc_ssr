# Sweets

## Dependencies
- [Node 6+](https://nodejs.org/en/download/)
- [Yarn](https://yarnpkg.com/lang/en/docs/install/)

## Running
1. Install JS dependencies
	```sh
	yarn install
	```
1. Generate bundles
	```sh
	yarn run build
	```
1. Start the app
	```sh
	yarn run start
