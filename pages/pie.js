import Link from 'next/link'
import Head from '../components/head'
import Nav from '../components/nav'

export default () => (
  <div>
    <Head
      title="Home-made apple pie"
      description="Home-made apple pie"
      keywords="Home-made apple pie, coffee, fruit pie, cup of java"
    />
    <Nav />

    <div className="hero">
      <h1 className="title">Home-made pie</h1>
      <h2 className="description">A generated text about home-made pies and coffee</h2>

      <p className="content">
        Coffee is fantastic by itself, but with coffee with something else - such as a dessert - is simply heavenly. The taste of java goes well with lots of things - such as biscuits, coffee cakes, constantly with whatever chocolate or each pie. All of us have their very own special recipe for their most favored type of pie. Obviously, there are all kinds of pies, like lemon meringue, peach, cherry, pecan, pumpkin. It seems that every kind of fruits has been made to a pie sooner or later or other. This pie is not really that challenging to master and when you've made it, you are certain to put in it to your favorites list too. 
      </p>
      <p className="content">
        Make this pie and possess a generous piece with your next cup of java, hopefully in a special coffee mug that's personalized to your very own interests. Particular glasses or cups that reflect your very own lifetime make enjoying your coffee and pie instant that much more gratifying. Egg Wash -composed of one beaten egg + little quantity of water. Preheat the oven to 450 F.Measure all pastry ingredients, except water to food processor. Pulse several times until mixture is the size of small peas. During top, add just enough cold plain water until dough forms a ball. Cut dough in about thirds- two thirds will be utilized for the bottom crust, and one 3rd for top. Roll out 2 thirds of dough on slightly Floured surface and line a pie plated along with dough. Sugar on bottom of crust. Sprinkle corn starch on apples, then 2 TBSP.
      </p>

      <div className="row">
        <Link href="/">
          <a className="card">
            <h3>Go back to home &rarr;</h3>
            <p>Home sweet home</p>
          </a>
        </Link>
        <Link href="/chocolate">
          <a className="card">
            <h3>Go to chocolate process &rarr;</h3>
            <p>
              Generated text about cocoa bean to chocolate
            </p>
          </a>
        </Link>
        <Link href="/cupcake">
          <a className="card">
            <h3>Go to cupcake recipe &rarr;</h3>
            <p>Generated text about chocolate cupcakes</p>
          </a>
        </Link>
      </div>
    </div>

    <style jsx>{`
      .hero {
        width: 100%;
        color: #333;
      }
      .title {
        margin: 0;
        width: 100%;
        padding-top: 80px;
        line-height: 1.15;
        font-size: 48px;
      }
      .title, .description {
        text-align: center;
      }
      .row {
        max-width: 880px;
        margin: 80px auto 40px;
        display: flex;
        flex-direction: row;
        justify-content: space-around;
      }
      .card {
        padding: 18px 18px 24px;
        width: 220px;
        text-align: left;
        text-decoration: none;
        color: #434343;
        border: 1px solid #9B9B9B;
      }
      .card:hover {
        border-color: #067df7;
      }
      .card h3 {
        margin: 0;
        color: #067df7;
        font-size: 18px;
      }
      .card p {
        margin: 0;
        padding: 12px 0 0;
        font-size: 13px;
        color: #333;
      }
      .content {
        max-width: 80%;
        margin: auto;
        margin-bottom: 20px;
      }
    `}</style>
  </div>
)
