import Link from 'next/link'
import Head from '../components/head'
import Nav from '../components/nav'

export default () => (
  <div>
    <Head
      title="Delicious chocolate cupcake"
      description="Delicious chocolate cupcake"
      keywords="Delicious chocolate cupcake, white chocolate, cupcake recipe, generated text"
    />
    <Nav />

    <div className="hero">
      <h1 className="title">Delicious chocolate cupcake</h1>
      <h2 className="description">
        A generated text containing a delicious chocolate cupcake recipe
      </h2>
      <p className="content">
        White chocolate cupcakes are a spectacular spectacle of restraint. These white chocolate cupcakes have a spectacular appearance and a subtle sweetness. The white chocolate flavour is not incredibly assertive-if whatever it's only a delightful vanilla undertone that goes perfectly using the fine crumbed feel to make a spare sophistication that appeases kids in addition to adults, such as those fussy foodies you invited into your next dinner party. Position an oven rack in the middle place and pre-heat the oven to 350F. Line twelve 3-by-1\/2 inch muffin cups with paper liners. Bring a skillet of water into a simmer over moderate heat. 
      </p>
      <p className="content">
        Turn the heat off and put the bowl from the warm water. Allow it stand, stirring occasionally, till the chocolate is totally melted and smooth. Be careful to not allow any water get from the bowl or the chocolate will grab and clump. Remove the bowl from the water and leave Cool until tepid. In a glass measuring cup, stir together the milk and vanilla. In the bowl of a stand mixer or from a big bowl using a handheld electric mixer, beat the butter and sugar on high speed till light and fluffy, about 3 minutes. Beat from the eggs, 1 at a time, scraping the sides of the bowl as necessary. 
      </p>
      <p className="content">
        Reduce the speed into low and beat in the flour mixture in 3 additions, alternating using the milk mixture in 2 additions, and blend till smooth. Using a 2 inch diameter ice cream scoop or a 1\/3 cup measuring cup, scoop the batter in the prepared cups. The cups should not be more than 2\/3 full.6. Bake until a wooden toothpick inserted into the centre of a cupcake comes out clean, 25 into half a hour. Do not be alarmed if the cupcakes do not rise appreciably or if they cannot turn golden brown. Let the cupcakes cool in the foil N a wire rack for 10 to fifteen minutes.
      </p>

      <div className="row">
        <Link href="/">
          <a className="card">
            <h3>Go back to home &rarr;</h3>
            <p>Home sweet home</p>
          </a>
        </Link>
        <Link href="/pie">
          <a className="card">
            <h3>Go to home-made apple pie &rarr;</h3>
            <p>Generated text about home made pies</p>
          </a>
        </Link>
        <Link href="/chocolate">
          <a className="card">
            <h3>Go to chocolate process &rarr;</h3>
            <p>
              Generated text about cocoa bean to chocolate
            </p>
          </a>
        </Link>
      </div>
    </div>

    <style jsx>{`
      .hero {
        width: 100%;
        color: #333;
      }
      .title {
        margin: auto;
        width: 100%;
        padding-top: 80px;
        line-height: 1.15;
        font-size: 48px;
      }
      .title, .description {
        text-align: center;
      }
      .row {
        max-width: 880px;
        margin: 80px auto 40px;
        display: flex;
        flex-direction: row;
        justify-content: space-around;
      }
      .card {
        padding: 18px 18px 24px;
        width: 220px;
        text-align: left;
        text-decoration: none;
        color: #434343;
        border: 1px solid #9B9B9B;
      }
      .card:hover {
        border-color: #067df7;
      }
      .card h3 {
        margin: 0;
        color: #067df7;
        font-size: 18px;
      }
      .card p {
        margin: 0;
        padding: 12px 0 0;
        font-size: 13px;
        color: #333;
      }
      .content {
        max-width: 80%;
        margin: auto;
        margin-bottom: 20px;
      }
    `}</style>
  </div>
)
