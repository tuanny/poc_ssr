import Link from 'next/link'
import Head from '../components/head'
import Nav from '../components/nav'

export default () => (
  <div>
    <Head
      description="Cocoa beans to chocolate"
      title="Cocoa beans to chocolate"
      keywords="Cocoa beans to chocolate, hot chocolate, chocolate process, cocoa beans, generated text"
    />
    <Nav />

    <div className="hero">
      <h1 className="title">Cocoa beans to chocolate</h1>
      <h2 className="description">A generated text about the process to transform cocoa beans to chocolate</h2>

      <p className="content">
        During the process beans temporarily germinate, but have been soon killed by heat, ethanol and lactic acid that permeate throughout the testa of the hot chocolate beans in their cotyledons as well as kill the embryo, the non germinating beans do not create a chocolate flavor. Biochemical transformations inside the bean cause the creation of numerous flavour precursors, ethanol could be answerable for the activation of specific hot chocolate bean cotyledon enzymes, acetic acid penetrates to the cocoa beans and thus the alkaloids and polyphenols residue from the fermenting hot chocolate beans to the neighboring pulp hence decreasing bitterness and astringency of the fermented hot chocolate beans, the pulp is transformed to a mild alcohol from its sugars as well as the proteins and polypeptides respond with polyphenols to provide a brown colour. Thus the process of fermentation is the cornerstone of the whole chocolate making procedure as well as now that the focus is on that the enhancement of flavour in the final chocolates during the usage of appropriate starter culture that enable the creation of equally fermented cocoa beans within four days in addition to produce standard bulk chocolates. 
      </p>
      <p className="content">
        The following stage is drying of that the fermented wet bean would be the low or volatile boiling acids, such as lactic acid are lost and the brown color becomes more conspicuous and result from the beans that are less astringent, but nevertheless bitter and then is the roasting stage that's important since it's the process where the complete flavor of chocolate begins coming out as well as the chemistry involved is hugely complicated since there is hundreds of compounds in that the seeds of that the chocolate so known as beans as well as the chemical reactions in this procedure are frequently concerning called theBrowning reactions. Then these beans have been ground and native way of doing it's in Mesoamerica that has been where chocolate was invented, they apply heat beneath while grounding which brings up the flavour of the beans and after that results from the creation of a solid chocolate mass.
      </p>

      <div className="row">
        <Link href="/">
          <a className="card">
            <h3>Go back to home &rarr;</h3>
            <p>Home sweet home</p>
          </a>
        </Link>
        <Link href="/pie">
          <a className="card">
            <h3>Go to home-made apple pie &rarr;</h3>
            <p>Generated text about home made pies</p>
          </a>
        </Link>
        <Link href="/cupcake">
          <a className="card">
            <h3>Go to cupcake recipe &rarr;</h3>
            <p>Generated text about chocolate cupcakes</p>
          </a>
        </Link>
      </div>
    </div>

    <style jsx>{`
      .hero {
        width: 100%;
        color: #333;
      }
      .title {
        margin: 0;
        width: 100%;
        padding-top: 80px;
        line-height: 1.15;
        font-size: 48px;
      }
      .title, .description {
        text-align: center;
      }
      .row {
        max-width: 880px;
        margin: 80px auto 40px;
        display: flex;
        flex-direction: row;
        justify-content: space-around;
      }
      .card {
        padding: 18px 18px 24px;
        width: 220px;
        text-align: left;
        text-decoration: none;
        color: #434343;
        border: 1px solid #9B9B9B;
      }
      .card:hover {
        border-color: #067df7;
      }
      .card h3 {
        margin: 0;
        color: #067df7;
        font-size: 18px;
      }
      .card p {
        margin: 0;
        padding: 12px 0 0;
        font-size: 13px;
        color: #333;
      }
      .content {
        max-width: 80%;
        margin: auto;
        margin-bottom: 20px;
      }
    `}</style>
  </div>
)
